# vHosts-creator

A small script to automate the creation of virtual hosts entries for Apache MAMP on macOS.

## Requirements

- Node v6
- MAMP
- macOS

## Install

```bash
$ npm install
```

## Run

```bash
$ sudo node index <host_name>
```

See all available options with:

```bash
$ node index --help
```

## Licence

MIT
