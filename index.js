#! /usr/bin/env node
'use strict';


const fs 			= require('fs');
const os 			= require('os');
const co 			= require('co');
const prompt 		= require('co-prompt');
const terminal 		= require('commander');
const hostsFile 	= '/etc/hosts';
const mampPath 		= '/Applications/MAMP/conf/apache/extra/httpd-vhosts.conf';
const brewPath 		= '/usr/local/etc/apache2/2.4/extra/httpd-vhosts.conf';
const username 		= os.userInfo().username;

let sslCertificatePath 	= '/private/etc/apache2/';
let vhostsFile 			= mampPath;
let hostsEntry;
let vhostEntry;


/**
 * Generate the hosts entry.
 */
terminal
	.arguments('<hostName>')
	.option('-b, --brew', 'If this is for the brew version of apache2')
	.option('-s, --ssl   <sslPath>', 'Set the SSL certificate path - defaults to: /private/etc/apache2/')
	.action((hostName) => {
		if (terminal.sslPath) sslCertificatePath = terminal.sslPath;

		// Check the vhosts file exists.
		if (fs.existsSync(vhostsFile) === false) {
			throw new Error(`httpd-vhosts.conf does not exist in: ${path}`);
		}

		// Check the ssl_certificate file exists.
		if (fs.existsSync(sslCertificatePath) === false) {
			throw new Error(`SSL certificate doesn't exist: ${sslCertificatePath}`);
		}

		// Hosts.
		hostsEntry = `127.0.0.1 ${hostName}`;

		// Virtual hosts.
		vhostEntry = `# Auto-generated: ${hostName}`
				   + `<Directory "/Users/${username}/Sites/${hostName}">`
				   + `    Allow From All`
				   + `    AllowOverride All`
				   + `</Directory>`
				   + `<VirtualHost *:80>`
			       + `    DocumentRoot "/Users/${username}/Sites/${hostName}"`
				   + `    ServerName ${hostName}`
				   + `</VirtualHost>`
				   + `<VirtualHost *:443>`
				   + `    DocumentRoot "/Users/${username}/Sites/${hostName}"`
				   + `    ServerName ${hostName}`
				   + `    SSLEngine on`
				   + `    SSLCertificateFile ${sslCertificatePath}server.crt`
				   + `    SSLCertificateKeyFile ${sslCertificatePath}server.key`
				   + `</VirtualHost>`;

		// Append the entry to the hosts file.
		try {
			fs.appendFileSync(hostsFile, hostsEntry);
		} catch (ex) {
			throw new Error(ex);
		}

		// Append the entry to the vhosts file.
		try {
			fs.appendFileSync(vhostsFile, vhostEntry);
		} catch (ex) {
			throw new Error(ex);
		}

		// Completed.
		console.log(`Success! Please restart Apache and then goto http://${hostName}`);
	})
	.parse(process.argv);
